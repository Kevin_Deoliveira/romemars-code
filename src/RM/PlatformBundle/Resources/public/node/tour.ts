module Tour {
	export class TourParTour {
		number_tour: number;
		type_play_game : number;
		constructor(private id_game: number, private id_player_1: string, private pseudo_1: string, private type_1: number, private id_player_2: string, private pseudo_2: string, private type_2: number) {
			this.number_tour = 1;
			this.type_play_game = 1;
	 }
    increase_tour() {
       this.number_tour ++;
    }
}

}

module.exports = Tour;


