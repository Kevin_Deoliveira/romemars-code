module Sayings {
    export class Warrior {
        x: number;
        y: number;
        id: number;
        move_case: boolean; //Si il a le droit de bouger
        attack_ok: boolean; // Variable si le perso peut attaquer , bloque les attack à la suite
        move_max: number; // Son dep max
        portee_max: number; // Sa portée max
        distance: boolean; //Si c'est un archer
        pts_life: number;
        pts_armure: number; // inutile
        pts_attack: number;
        pts_defense: number;
        pts_attack_distance: number;
        pts_defense_distance: number;
        attaquant: boolean; //Permet de savoir qui attaque
        url_avatar:string;
        url_icone:string;
        attaquant_player: boolean; //Permet de connaitre les unite de lattaquant

        constructor(public type_unity: string) {
            this.x = 0;
            this.y = 0;
            this.id;
            this.move_case = true;
            this.pts_life = 100;
            this.attaquant = false;
            this.attaquant_player = false;
            this.url_icone;
            this.url_avatar;
            this.attack_ok = true;
        }
        set_y(position_y: number) {
            this.y = position_y;
        }
        get_y() {
            return this.y;
        }
        set_x(position_x: number) {
            this.x = position_x;
        }
        get_x() {
            return this.x;
        }
        avatar_deplacable_true(){
            this.move_case = true;
        }
        avatar_deplacable_false(){
            this.move_case = false;
        }
        avatar_attack_true(){
            this.attack_ok = true;
        }
        avatar_attack_false(){
            this.attack_ok = false;
        }
    }

    export class Peasants extends Warrior {
        constructor(type_unity: number) {
            super(type_unity);
            this.move_max = 4;
            this.pts_armure = 20 // inutile pour le moment
            this.pts_attack = 25;
            this.pts_defense = 20;
            this.pts_attack_distance = 0;
            this.pts_defense_distance = 15;
            this.distance = false;
            this.portee_max = 0;

        }

    }

    export class Town_Watch extends Warrior {
        constructor(type_unity: number) {
            super(type_unity);
            this.move_max = 3;
            this.pts_armure = 35
            this.pts_attack = 35;
            this.pts_defense = 35;
            this.pts_attack_distance = 0;
            this.pts_defense_distance = 30;
            this.distance = false;
            this.portee_max = 0;

        }

    }
    export class Town_Archer_Auxilia extends Warrior {
        constructor(type_unity: number) {
            super(type_unity);
            this.move_max = 4;
            this.pts_armure = 35
            this.pts_attack = 15;
            this.pts_defense = 15;
            this.pts_attack_distance = 25;
            this.pts_defense_distance = 30;
            this.distance = true;
            this.portee_max = 4;


        }

    }
    export class Archer_Watch extends Warrior {
        constructor(type_unity: number) {
            super(type_unity);
            this.move_max = 3;
            this.pts_armure = 20
            this.pts_attack = 15;
            this.pts_defense = 20;
            this.pts_attack_distance = 25;
            this.pts_defense_distance = 15;
            this.distance = true;
            this.portee_max = 2;

        }

    }
}

module.exports = Sayings;






