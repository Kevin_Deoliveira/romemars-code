<?php

// src/OC/PlatformBundle/Controller/AdvertController.php

namespace RM\PlatformBundle\Controller;

// N'oubliez pas ce use :
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use RM\PlatformBundle\Entity\Login;
use RM\PlatformBundle\Entity\Message;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Util\SecureRandom;
use Symfony\Component\HttpFoundation\Session\Session;

class AdvertController extends Controller{
    
  public function indexAction(Request $request){
       // Notre liste d'annonce en dur
    $listAdverts = array(
      array(
        'title'   => 'Présentation de RomeMars',
        'id'      => 1,
        'author'  => 'Kevin',
        'content' => 'Après plusieurs mois de développement la première version de RomeMars est disponible dans une version Beta.
            RomeMars est un jeu indépendant crée par ...',
        'date'    => new \Datetime(),
        'slug'    => 'lancement-romemars')

    );


    return $this->render('RMPlatformBundle:Advert:index.html.twig', array(
      'listAdverts' => $listAdverts
    ));
  }
  public function loginAction(Request $request){
      $session = new Session();
        // Si deja co on le remet sur le room
      if($session->has('pseudo')){
          return $this->redirectToRoute('rm_platform_room');
      }
      
   $advert = new Login();

    // On cr�e le FormBuilder gr�ce au service form factory
    $form= $this->get('form.factory')->createBuilder('form', $advert)
      ->add('Pseudo',     'text', array('max_length' => 20))
      ->add('Valider',      'submit')
      ->getForm()      
    ;
        // On fait le lien Requ�te <-> Formulaire
    // � partir de maintenant, la variable $advert contient les valeurs entr�es dans le formulaire par le visiteur
    $form->handleRequest($request);

    $this->get('session')->clear();

    if ($form->isValid()) {
        
        $characts    = 'abcdefghijklmnopqrstuvwxyz';
        $characts   .= 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';	
        $characts   .= '1234567890';
        $code_aleatoire      = '';
        for($i=0;$i < 10;$i++)    //10 est le nombre de caract�res
        {
                 $code_aleatoire .= substr($characts,rand()%(strlen($characts)),1);
        }
        
        
        $session = $request->getSession();
        $pseudo =  $advert->getPseudo();
       
        
        
        // d�finit et r�cup�re des attributs de session
        $session->set('pseudo', $pseudo);
        $session->set('id_generate', $code_aleatoire);
        $session->getFlashBag()->add('info', 'Bonjour legionnaire '.$pseudo);

         return $this->redirect($this->generateUrl('rm_platform_room'));
    }
      

       return $this->render('RMPlatformBundle:Advert:login.html.twig', array(
            'form' => $form->createView()

    ));
 }
  public function roomAction(){

      $session = new Session();
      //On détruit les sessions de jeux si présentes
      if($session->has('id_game')){
          $session->remove('id_game');
          $session->remove('type');
      }

      if(!$session->has('id_generate')){
          return $this->redirectToRoute('rm_platform_login');
      }

      $advert = new Message();

      // On cr�e le FormBuilder gr�ce au service form factory
      $form= $this->get('form.factory')->createBuilder('form', $advert)
          ->add('message',     'text')
          ->add('valider',      'submit')
          ->getForm()
      ;

      return $this->render('RMPlatformBundle:Advert:room.html.twig', array(
          'form' => $form->createView()

      ));


 }

 
 
   public function teamAction($id){
       
    $session = new Session();
    //$session->start();
    if(!$session->has('id_game')){
        $tab_idGame = explode("-", $id);
        $session->set('id_game', $tab_idGame[0]);
        $session->set('type', $tab_idGame[1]);
    }
    if(!$session->has('id_generate')){
           return $this->redirectToRoute('rm_platform_login');
    }

 
       return $this->render('RMPlatformBundle:Advert:team.html.twig', array(
     
    ));
 }
   public function mapAction(){

       $session = new Session();

       if(!$session->has('id_generate')){
           return $this->redirectToRoute('rm_platform_login');
       }

          // $session->getFlashBag()->add('info', 'Votre adversaire à quitté la partie ce lache.');


       //04 = herbe
       //02 = trou
        return $this->render('RMPlatformBundle:Advert:map.html.twig', array(
            'map' => '01,01,01,01,04,01,01,01,04,01:01,01,01,01,01,01,01,01,01,01:01,01,01,01,01,04,01,01,01,01:'
                . '03,01,02,01,04,01,02,01,01,03:03,01,01,01,02,01,01,01,01,03:03,01,01,04,02,04,01,01,01,03:'
                . '03,01,02,01,04,01,02,01,01,03:01,01,01,01,01,01,01,01,01,01:01,01,01,01,01,01,01,01,01,01:'
                . '01,01,01,04,01,01,01,04,01,01'
        ));

    }
    public function messageAction($id){

        $session = new Session();

        if($id == 1){
                $session->getFlashBag()->add('info', 'Félicitation pour votre victoire.');
        }
        if($id == 2){
            $session->getFlashBag()->add('info', "Ça s'est joué de peux vous devriez réesayer");
        }
        if($id == 3){
            $session->getFlashBag()->add('info', "Vous ou votre adversaire n'ont pas choisis vos unités dans le temps imparti.");
        }
        /*if($id == 1){
            $session->getFlashBag()->add('info', 'Votre adversaire à quitté la partie ce lache.');
        }*/


        echo true;
        exit;

    }
    public function helpAction(){
        return $this->render('RMPlatformBundle:Advert:help.html.twig', array(
        ));
    }

    public function galerieAction(){

        return $this->render('RMPlatformBundle:Advert:galerie.html.twig', array(
        ));
    }

    public function blogAction($id,$slug){


        if($id = 1){

           $article = " ";


        }

        $article = array(
            array(
                'title'   => 'Présentation de RomeMars',
                'id'      => 1,
                'author'  => 'Admin',
                'content' => 'Après plusieurs mois de développement la première version de RomeMars est disponible dans une version Beta.
            RomeMars est un jeu indépendant crée par un passionné de développement et d’histoire. Cette première version à pour but de faire découvrir l’univers romain à d’autres personnes et d’avoir des premiers retours d’experience.

            Le concept est simple une carte avec deux joueurs qui contrôlent leurs unités et se déplacent en temps réel. Il était important de ne pas créer un jeu ou il faut attendre 24h pour passer un tour. Ici il suffit de valider son tour pour que le joueur adverse puissent déplacer ses unités.

            Un des autres avantages est la possibilité de jouer sans inscription, aucune donnée n’est enregistrée dans cette version beta.

            J’espère que cette première version de RomeMars plaira aux joueurs afin de créer une communauté autour du jeu et qu’ensemble nous puissions faire évoluer RomeMars, pour qu’il devienne un ténor du jeu de stratégie par navigateur (sic).
       ',
                'date'    => new \Datetime()),
        );

        return $this->render('RMPlatformBundle:Advert:blog.html.twig', array(
            'article' => $article

        ));
    }

    public function partenairesAction(){

        return $this->render('RMPlatformBundle:Advert:partenaires.html.twig', array(
        ));
    }



}
