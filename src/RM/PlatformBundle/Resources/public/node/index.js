var socket = require( 'socket.io' );
var express = require( 'express' );
var http = require( 'http' );
var app = express();


var server = http.createServer( app );

var io = socket.listen( server );
var agx = require('./nodeServer');


// Listen for Socket.IO Connections. Once connected, start the game logic.
io.sockets.on('connection', function (socket) {
    agx.initGame(io, socket);
});

server.listen( 8181 );

