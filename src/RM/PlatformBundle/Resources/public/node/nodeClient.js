jQuery(function($){    
    'use strict';

    /**
     * All the code relevant to Socket.IO is collected in the IO namespace.
     *
     * @type {{init: Function, bindEvents: Function, onConnected: Function, onNewGameCreated: Function, playerJoinedRoom: Function, beginNewGame: Function, onNewWordData: Function, hostCheckAnswer: Function, gameOver: Function, error: Function}}
     */
    var IO = {

        /**
         * This is called when the page is displayed. It connects the Socket.IO client
         * to the Socket.IO server
         */
        init: function() {
            IO.socket = io.connect('http://localhost:8181');
            IO.bindEvents();
        },

        /**
         * While connected, Socket.IO will listen to the following events emitted
         * by the Socket.IO server, then run the appropriate function.
         */
        bindEvents : function() {
            IO.socket.on('connected', IO.onConnected );
            
            IO.socket.on('firstPlayer', IO.firstPlayer );
            IO.socket.on('newGameCreated', IO.onNewGameCreated );
            IO.socket.on('responseAskBattle', IO.onResponseAskBattle );
           
        },

        /**
         * The client is successfully connected!
         */
        onConnected : function() {
            console.log('je suis co')
           //On envois vers le serveur l'id session unique du client
          // console.log(IO.socket.socket.sessionid);
            App.mySocketId = IO.socket.socket.sessionid; // on garde une copie de l'id sur le client
          //  IO.socket.emit('hostCreateNewGame'); 
        },
        onNewGameCreated : function(data) {
            console.log('Come on');
            console.log(data);
           for(var i in data) {
               if( App.mySocketId == data[i].mySocketId ){
                      document.location.href="http://localhost/map-rome/map.php?id="+App.mySocketId+"&gameid="+data[i].gameId+"&type="+data[i].type;
                      return false;
               }
            }

        },
        firstPlayer : function(data) {
            IO.socket.emit('hostCreateNewGame');
        },
        onResponseAskBattle : function() {
            console.log('je suis co')
           //On envois vers le serveur l'id session unique du client
          // console.log(IO.socket.socket.sessionid);
            App.mySocketId = IO.socket.socket.sessionid; // on garde une copie de l'id sur le client
          //  IO.socket.emit('hostCreateNewGame'); 
        },

    };
           //     IO.socket.emit('hostCreateNewGame');

    var App = {

        init: function () {
            App.bindEvents();

        },


        /**
         * Create some click handlers for the various buttons that appear on-screen.
         */
        bindEvents: function () {
            App.$doc = $(document);
            // Host
          //  App.$doc.on('click', '.player', App.Host.onCreateClick);
            App.$doc.on('click', '.messi-closebtn', App.Host.onDeleteAlert);
         //   App.$doc.on('click', '.mod-button', App.Host.onAcceptAlert);
        },


        /* *******************************
           *         HOST CODE           *
           ******************************* */
        Host : {

    
            onCreateClick: function () {
         //     App.mySocketId = IO.socket.socket.sessionid; // on garde une copie de l'id sur le client
         //     console.log(IO.socket.socket.sessionid);
//             $( "body" ).append( '<div class="messi-modal" style="opacity: 0.4; width: 1903px; height: 921px; z-index: 99999;"></div>' );
//             $( "body" ).append( '<div class="messi" style="top: 237.5px; left: 786px; z-index: 100000; opacity: 1;"><div class="messi-box"><div class="messi-wrapper"><div class="messi-titlebox"><a class="messi-closebtn"><i class="fa fa-times"></i></a><span class="messi-title">Supprimer un point de vente</span></div><div class="messi-content" style="height: auto;"><p class="messi-warning"><i class="icon-warning-sign icon-3x pull-left"></i>Êtes vous sur de vouloir le supprimer?<br><strong>Cette action est irréversible!!!</strong></p></div><div class="messi-footbox clearfix"><div class="messi-actions clearfix">\n\
//            <div class="btnbox"><a class="mod-button "><i class="icon-ok"></i> Supprimer</a></div></div></div></div></div></div>' );
//         
       
            },
             onDeleteAlert: function () {
              $( ".messi-modal" ).remove();
              $( ".messi" ).remove();
            },
             onAcceptAlert: function () {
               console.log( this);
               console.log($('.player').attr('data'));
               App.Host.onDeleteAlert();
               IO.socket.emit('askBattle', $('.player').attr('data'));
          
            }

        }

    };

    IO.init();
    App.init();

}($));
