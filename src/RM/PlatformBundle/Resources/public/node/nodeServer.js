var io;
var gameSocket;
var nb_player = 0;
var gameId;
var tab_game = [];

var choose_player = []; // contient l'id du game / du player / et son choix d'armée
var tab_players = [];//contient les infos des joueurs connecté
var tab_compteur_team = [];// contient le time du compteur de choix de team et d'attente du joueur
var player_online_map = [];// permet de savoir si les deu adv sont bien sur la page de map
var nb_tour = [];// contient le nb de tour l'id game et l'id du joueur qui doit jouer
var tab_message = [];// Contient la liste des messages


var obj_tour = [];// Contient les obj des tours de chaque partie

var list_component_send = [];// contient la liste des adversaire qu'on a demandé en attack
var list_component_receive = [];// contient la liste des demandes de combat
var list_component_accept = [];// contient la liste des combats accepté


var class_warriors = require('./warriors'); // appel de la classe qui gère les avatars
var class_tour = require('./tour'); // appel de la classe qui gère les avatars

var asynchrone = false;



exports.initGame = function(sio, socket){


    io = sio;
    gameSocket = socket;
    /*
     * Partie Room
     */
    gameSocket.on('firstConnect', firstConnect);
    gameSocket.on('askBattle', askBattle);
    gameSocket.on('sendRequest', sendRequest);
    gameSocket.on('acceptFight', acceptFight);

    gameSocket.on('get_attack_send', getAttackSend);
    gameSocket.on('get_attack_receive', get_attack_receive);

    gameSocket.on('removeFight_send', removeFightsend);
    gameSocket.on('removeFight_receive', removeFightReceive);

    gameSocket.on('get_attack_accept', getAttackAccept);
    gameSocket.on('remove_attack_accept', removeAttackAccept);

    //gameSocket.on('hostCreateNewGame', hostCreateNewGame);
    gameSocket.on('setPosition', setPosition);
    gameSocket.on('hostRoomFull', hostPrepareGame);
    gameSocket.on('initSocket', initSocket);
    gameSocket.on('return_attack', return_attack);
    gameSocket.on('lie_attack_socketGame', lie_attack_socketGame);
    gameSocket.on('lie_attack_socketGame_player', lie_attack_socketGame_player);



    /*
     * Partie Team
     */
    gameSocket.on('sendListWarriors', sendListWarriors);
    gameSocket.on('compteur', compteur);
    gameSocket.on('failCompteur', failCompteur);//Si la personne n'as pas choisi sa team dans le temps impartie
    gameSocket.on('stopGame', stopGame);//Suppresion de la partie
    gameSocket.on('remove_fight_accept', remove_fight_accept);//On supprime la partie une fois accepté
    gameSocket.on('remove_fight_accept_attack', remove_fight_accept_attack);//On supprime la partie une fois accepté


    /*
     * Partie Room
     */
    gameSocket.on('set_armies', set_armies);
    /*
     * Partie Map
     */
    gameSocket.on('sendColision', sendColision);


    gameSocket.on('initCompteur', initCompteur);
    gameSocket.on('get_position_choice', getPositionChoice);
    gameSocket.on('positionOfUnity', positionOfUnity);
    gameSocket.on('setPositionUnity', setPositionUnity);
    gameSocket.on('collision', collision);
    gameSocket.on('setPlayerBegin', setPlayerBegin);
    gameSocket.on('compteurTourParTour', compteurTourParTour);
    gameSocket.on('validateTour', validateTour);
    gameSocket.on('passTour', passTour);

    gameSocket.on('info_tipShow', infoTipShow);

    /*
     * Partie Autre
     */


    gameSocket.on('controlPlayerOnline', controlPlayerOnline);

    /*
     * Partie Message
     */

    gameSocket.on('send_message', send_message);
    gameSocket.on('getMessage', getMessage);
    gameSocket.on('newPlayer', newPlayer);



    gameSocket.on('disconnect', function() {
        console.log('Got disconnect!');


        /* var i = allClients.indexOf(socket);
         allClients.splice(i, 1);*/

        var tail_tab = tab_players.length;
        for(var i = 0; i <= tail_tab-1;i++ ){
            if(socket.id == tab_players[i]['id'] ){

                var id_Player = tab_players[i].id_generate;

                console.log('tab_players')
                console.log(tab_players)
                console.log('tab_players')

                tab_players.splice(i,1);
                io.sockets.emit('listPlayer',tab_players);

                console.log('id_Player');
                console.log(id_Player);
                console.log('id_Player');


                console.log('choose_player');
                console.log(choose_player);
                console.log('choose_player');
                var info;
                var is_play = choose_player
                    .filter(function (el) {
                        info = el.idGame;
                        return el.idPlayer == id_Player;
                    }
                ).length;
                console.log('is_play')
                console.log(is_play)
                console.log('is_play')

                //Il joue on verifie si il est tjr la sinon on supprime la partie
                if(is_play){

                    console.log('info')
                    console.log(info)
                    console.log('info')

                     controlPlayerOnline(id_Player,info);

                }


                break;
            }
        }



    });
}



//Contient lid de la personne qui a quité ou actualisé
function controlPlayerOnline(data,idGame) {

    var obj = this;
    console.log('on verifie si il a quitté')
    var info;

    setTimeout(function(data2){

        var nb_player = tab_players.filter(function(val){
            if(val.id_generate == data){
                info = val;
                return val.id_generate ;
            }
        }).length;

        var tab_end = {idGame : idGame} ;


        //Ladv est plus present on le signal au joueur restant
        if( nb_player <= 0 ){

            io.sockets.in(idGame).emit('endGame',{value : 1});
            stopGame(tab_end);
            console.log('stop game plus ici')

        }else{
            //Si il est co mais plus sur la bonne page on coupe la partie

            //Assez incomprehensible le != ne veut pas passer donc en mode else sale
            if(info.url == "map" || info.url == "team" ){

            }else{
                console.log('termine')
                  io.sockets.in(idGame).emit('endGame',{value : 1});

                  stopGame(tab_end);
                console.log('stop game plus sur map')


            }
        }

    }, 25000);

};






function send_message(data){

    console.log(data)

    date = new Date();
    data.heure = date.getHours();
    data.minute = date.getMinutes();
    data.news = 0; // cest pas un new player

    if (data.heure<10) {data.heure = "0" + data.heure}
    if (data.minute<10) {data.minute = "0" + data.minute}

    tab_message.push(data);
    if(tab_message.length > 15){
        tab_message.pop();
    }
    io.sockets.emit('show_message',  data );

}

function getMessage(){



    this.emit('return_message',  tab_message );

}
function newPlayer(data){

    console.log('newPlayer')
    console.log('newPlayer')
    console.log('newPlayer')
    date = new Date();
    data.heure = date.getHours();
    data.minute = date.getMinutes();
    data.news = 1; // cest pas un new player

    if (data.heure<10) {data.heure = "0" + data.heure}
    if (data.minute<10) {data.minute = "0" + data.minute}

    if(tab_message.length > 15){
        tab_message.pop();
    }

    console.log(data);
    io.sockets.emit('show_message',  data );

}



/* *******************************
 *                             *
 *       HOST FUNCTIONS        *
 *                             *
 ******************************* */


function firstConnect(data_player) { // quand un joueur se co on garde ses infos son id et pseudo
    var found = false;
    var tail_tab = tab_players.length;

    var date = new Date();
    var launch = new Date(date.getFullYear(),date.getMonth(),date.getDate(),date.getHours(),date.getMinutes()-2,date.getSeconds());

    data_player['id'] = gameSocket.id;
    data_player['time'] = launch;


    tab_players.push(data_player);


    this.join(data_player.id_generate); // je lie la socket � l'id du joueur
    io.sockets.emit('listPlayer',tab_players);// on envoi la communication sur la bonne chambre qui correspond au game id


};

/*
 * Demande de combat à un adv data contien l'id generé + le pseudo de l attaquant
 */
function askBattle(data){

    var find = false;
    if(data.id != data.id_attaque ){ //Controle qu'on ne s'attaque pas
        if(compteur_attack_send(list_component_send,data.id_attaque)){//On test que l'attaquant n'a pas deja envoyé 3 attaques
            if(!in_array_same_attack_def(list_component_receive, data.id_attaque,data.id)){ // On verifie que l'attaquant n'a pas deja été attaqué par le defenseur
                if(in_array_attack(list_component_send, data.id_attaque)){ //Si l'attaquant est dans deja dans la liste on verifie que la personne qu'il attaque l'est deja ou pas
                    if(!in_array_fight(list_component_send, data.id_attaque,data.id)){//Si on le trouve pas on l'ajoute sinon on passe ce qui veut dire qu'il a deja envoyée une requete donc on passe
                        var name = findInformation(data.id);
                        var data_2 = {
                            id_attaque : data.id_attaque, //id de la personne qui attaque
                            id_receive : data.id, // id de celle qui recois
                            name_receive : name['pseudo'] // name de la personne qui est attaque
                        };
                        list_component_send.push(data_2);

                        find = true;

                    }
                }else{
                    var name = findInformation(data.id);
                    var data_2 = {
                        id_attaque : data.id_attaque, //id de la personne qui attaque
                        id_receive : data.id, // id de celle qui recois
                        name_receive : name['pseudo'] // name de la personne qui est attaque
                    };
                    list_component_send.push(data_2);

                    find = true;
                }
                if(find){
                    this.broadcast.to(data.id).emit('responseAskBattle',{pseudo_att :data.pseudo_attack, id_att : data.id_attaque });
                }
            }else{
                var message = '<div class="alert alert-dismissible notif "> <button class="close" data-dismiss="alert">&times;</button> Ce joueur vous a déjà envoyée une attaque penser à vérifier dans la liste des attaques reçu</div>';
                this.emit('list_error',{value : 1, message : message});



            }
        }else{
            var message = '<div class="alert alert-dismissible notif "> <button class="close" data-dismiss="alert">&times;</button> Vous avez le droit à 3 attaques simultanées</div>';
            this.emit('list_error',{value : 1, message : message});
        }
    }

};
/*
 * Demande de combat à un adv data contien l'id generé + le pseudo de l attaquant
 */

function sendRequest(data_receive){
    var find = false;
    var name_ok;

    if(data_receive.id != data_receive.id_attaque ){ //Controle qu'on ne s'attaque pas
        if(compteur_attack_receive(list_component_receive,data_receive.id_attaque)){//On test que l'attaquant n'a pas deja envoyé 3 attaques
            if(!in_array_same_attack_def(list_component_receive, data_receive.id_attaque,data_receive.id)){ // On verifie que l'attaquant n'a pas deja été attaqué par le defenseur
                if(in_array_attack(list_component_receive, data_receive.id_attaque)){ //Si l'attaquant est dans deja dans la liste on verifie que la personne qu'il attaque l'est deja ou pas
                    if(!in_array_fight(list_component_receive, data_receive.id_attaque,data_receive.id)){//Si on le trouve pas on l'ajoute sinon on passe ce qui veut dire qu'il a deja envoyée une requete donc on passe
                        var name = findInformation(data_receive.id_attaque);
                        var data_3 = {
                            id_request : data_receive.id, //id de la personne qui attaque
                            id_ask : data_receive.id_attaque, // id de celle qui recois
                            name_ask : name['pseudo'] // name de la personne qui est attaque
                        };
                        name_ok = name['pseudo'];
                        list_component_receive.push(data_3);


                        find = true;

                    }
                }else{
                    var name = findInformation(data_receive.id_attaque);
                    if (typeof  name['pseudo'] != 'undefined'){
                        var data_3 = {
                            id_request : data_receive.id, //id de la personne qui attaque
                            id_ask : data_receive.id_attaque, // id de celle qui recois
                            name_ask : name['pseudo'] // name de la personne qui est attaque
                        };
                        name_ok = name['pseudo'];
                        list_component_receive.push(data_3);
                        find = true;
                    }

                }

                if(find){
                    getAttackSend({ id_generate: data_receive.id_attaque,direct :1,obj:this });
                }
            }
        }
    }

};


/*
 * Le defenseur accept la bataille on l'envoie sur le choix de son armée avc un id precis
 * @param data / contient l'id de lattaquant
 */
function acceptFight(data) {

    var gameId = ( Math.random() * 100000 ) | 0; // on genere un game id aleatoire
    console.log('gameId')
    console.log(gameId)
    console.log('gameId')
    this.join(gameId.toString()); // je lie la socket au gameID
    var data_1 = {
        gameId : gameId,
        id_player : data.id_defense,
        type : 0 // signifie en mode def
    };
    choose_player.push(data_1);


    this.emit('choose_army',{game:gameId, id_attaque:data.id_attack} );// on envois le defenseur sur la page avec l id du game + l'id de lattaquant pour lui signaler que le
    // à accepter

};
/*
 * Le defenseur refuse la bataille on l'envoie sur le choix de son armée avc un id precis
 * @param data / contient l'id de lattaquant
 */
function removeFightsend(data) {

    console.log('list_component_send')
    console.log(list_component_send)
    list_component_send = list_component_send
        .filter(function (el) {
            return el.id_receive !== data.id_defense  ;
        });
    console.log('list_component_send')

    console.log(list_component_send)

    var message;
    if(data.status == 1){
        message = '<div class="alert alert-dismissible notif"> <button class="close" data-dismiss="alert">&times;</button> La demande de combat à bien été supprimée</div>';
        this.emit('list_error',{value : 2, message : message});

    }
    if(data.status == 2){
        message = '<div class="alert alert-dismissible notif"> <button class="close" data-dismiss="alert">&times;</button> La demande de combat à été refusée</div>';
        this.broadcast.to(data.id_attack).emit('list_error',{value : 2, message : message});
    }


};
/*
 * Le defenseur accept la bataille on l'envoie sur le choix de son armée avc un id precis
 * @param data / contient l'id de lattaquant
 */
function removeFightReceive(data) {
    console.log('list_component_receive')

    console.log(list_component_receive)
    console.log(data);
    console.log('data');


    list_component_receive = list_component_receive
        .filter(function (el) {

            return el.id_ask !== data.id_attack ;
        });

    var message;
    if(data.status == 1){
        message = '<div class="alert alert-dismissible notif"> <button class="close" data-dismiss="alert">&times;</button>Une demande d\'attaque a été supprimé</div>';
        this.broadcast.to(data.id_defense).emit('list_error',{value : 2, message : message});


    }
    if(data.status == 2){
        message = '<div class="alert alert-dismissible notif"> <button class="close" data-dismiss="alert">&times;</button> Vous avez bien refusé ce combat.</div>';
        this.emit('list_error',{value : 2, message : message});
    }


};
/*
 * Quand le defenseur a accepté on supprime la demande
 * @param data /
 */
function remove_fight_accept(data) {

    list_component_receive = list_component_receive
        .filter(function (el) {

            return el.id_request !== data.id_defense ;
        });


};
/*
 * Quand lattaquant a rejoint on supprime sa demande
 * @param data /
 */
function remove_fight_accept_attack(data) {

    console.log('list_component_send')
    console.log(list_component_send)
    console.log('list_component_send')


    console.log('data')
    console.log(data)

    console.log('data')



    list_component_send = list_component_send
        .filter(function (el) {

            return el.id_attaque !== data.id_attack ;
        });


    console.log('list_component_send')
    console.log(list_component_send)

    console.log('list_component_send')


};
/*
 * On signal à l'attaquant que le defenseur à accepter le combat
 * param : data contient l'id de lattaquant + l id du game
 */
function return_attack(data){
    list_component_accept.push(data);

    this.broadcast.to(data.id_attack).emit('notif_attack',{id_attaque:data.id_attack, game:data.gameID, pseudo_def:data.pseudo_defense});
}
/*
 * Renvois la liste des attaques accepté pa le defenseur
 * param : data contient l'id de lattaquant + l id du game
 */
function getAttackAccept(data){



    list_component_accept_player = list_component_accept
        .filter(function (el) {
                return el.id_attack == data.id_generate;
            }
        );

    this.emit('list_attack_accept', list_component_accept_player);

}
/*
 * Renvois la liste des attaques accepté pa le defenseur
 * param : data contient l'id de lattaquant + l id du game
 */
function removeAttackAccept(data){


    list_component_accept = list_component_accept
        .filter(function (el) {
                return el.id_defense != data.id_defense;
            }
        );


    this.emit('list_attack_accept', list_component_accept);

}
/*
 * On signal à l'attaquant que le defenseur à accepter le combat
 * param : data contient l'id de lattaquant + l id du game
 */
function getAttackSend(data){
    var resultat = [];
    if(in_array_attack(list_component_send,data.id_generate)){ //Si l'attaquant est dans deja dans la liste on verifie que la personne qu'il attaque l'ai deja ou pas
        for(var i= 0; i < list_component_send.length; i++){
            if(list_component_send[i].id_attaque == data.id_generate ){
                resultat.push(list_component_send[i])
            }
        }
    }


    if(!data.direct){
        this.emit('list_attack_send',resultat );
    }else{
        //Petite methode pour contourner le this car on peut faire un apel a partir d'une methode ce qui casse le this
        data.obj.emit('list_attack_send',resultat );
    }

}
/*
 * On signal à l'attaquant que le defenseur à accepter le combat
 * param : data contient l'id de lattaquant + l id du game
 */
function get_attack_receive(data){
    var resultat = [];

    if(in_array_defense(list_component_receive,data.id_generate)){ //Si l'attaquant est dans deja dans la liste on verifie que la personne qu'il attaque l'ai deja ou pas
        for(var i= 0; i < list_component_receive.length; i++){
            if(list_component_receive[i].id_request == data.id_generate ){
                resultat.push(list_component_receive[i])
            }
        }

    }
    this.emit('list_attack_receive', resultat);

}
/*
 * On signal à l'attaquant que le defenseur à accepter le combat
 * param : data contient l'id de lattaquant + l id du game
 */
function findInformation(id){
    //Renvois tte les infos par rapport à l'id

    var resultat = tab_players.filter(function(val){
        if(val.id_generate === id){
            return val;

        }
    })

    return resultat[0]; //Retoune le 1e resultat
}
/*
 * On signal à l'attaquant que le defenseur à accepter le combat
 * param : data contient l'id de lattaquant + l id du game
 */
function compteur_attack_send(array,search){
    var nb_attack = array.filter(function(val){
        if(val.id_attaque === search){
            return val.id_attaque === search;
        }
    }).length;
    console.log(nb_attack);//3
    if(nb_attack >= 3){
        return false;
    }
    return true;
}
/*
 * On signal à l'attaquant que le defenseur à accepter le combat
 * param : data contient l'id de lattaquant + l id du game
 */
function compteur_attack_receive(array,search){
    var nb_attack = array.filter(function(val){
        if(val.id_attaque === search){
            return val.id_attaque === search;
        }
    }).length;
    console.log(nb_attack);//3
    if(nb_attack >= 3){
        return false;
    }
    return true;
}
/*
 * On signal à l'attaquant que le defenseur à accepter le combat
 * param : data contient l'id de lattaquant + l id du game
 */
function in_array_attack(array, id) {

    if(array.length){ //Test que le tab contient quelque chose
        for(var i=0;i<array.length;i++) {
            if(array[i].id_attaque == id){
                return true;
            }
        }
    }

    return false;
}
/*
 * On signal à l'attaquant que le defenseur à accepter le combat
 * param : data contient l'id de lattaquant + l id du game
 */
function in_array_defense(array, id) {
    if(array.length){ //Test que le tab contient quelque chose
        for(var i=0;i<array.length;i++) {
            if(array[i].id_request == id){
                return true;
            }
        }
    }

    return false;
}
/*
 * On signal à l'attaquant que le defenseur à accepter le combat
 * param : data contient l'id de lattaquant + l id du game
 */
function in_array_fight(array, id_attack,id_defenseur) {

    for(var i=0;i<(array.length);i++) {
        if(array[i].id_attaque == id_attack && array[i].id_receive == id_defenseur ){
            return true;

        }

    }
    return false;
}
/*
 * Metthode qui controle si l'attaquant n'a pas deja été attaqué par le defenseur
 * param : data contient l'id de lattaquant + l id du game
 */
function in_array_same_attack_def(array, id_attack,id_defenseur) {
    for(var i=0;i<(array.length);i++) {
        if( array[i].id_ask == id_defenseur && array[i].id_request == id_attack ||  array[i].id_ask == id_attack && array[i].id_request == id_defenseur  ){
            return true;
        }
    }
    return false;
}


function sendListWarriors(data){
    console.log('demande bien recu')
    console.log(data);
    this.join('1234')


};

/**
 * Retourne la position de lavatar et d'autre info qui est envoyé sur pp3diso
 */
function getPositionChoice(data){
    var tail_tab = choose_player.length;

    for (var i = 0; i <= (tail_tab-1); i++) {
        if(data.idPlayer == choose_player[i].idPlayer && data.idGame == choose_player[i].idGame  ){
            var tail_tab_unities = choose_player[i].choice.length;
            for (var ii = 0; ii <= (tail_tab_unities)-1; ii++) {
                if(choose_player[i].choice[ii].id == data.idChoice ){
                    //On controle qu'il ne soit pas mort
                    if(choose_player[i].choice[ii].pts_life > 0 ){
                        this.emit('receive_position_choice',choose_player[i].choice[ii]);// on envois les infos pour rediriger sur la map de combat
                        break;
                    }

                }
            }
        }
    }



};

/**
 * The 'START' button was clicked and 'hostCreateNewGame' event occurred.
 */
function hostCreateNewGame(){
    //  nb_player +=1;
    if(nb_player <= 1){
        gameId = ( Math.random() * 100000 ) | 0; // on genere un game id aleatoire

        var data = {
            mySocketId : this.id,
            gameId : gameId,
            type : 'attack'
        };
        tab_game.push(data); // on stock dans un tableau
        this.join(gameId.toString()); // je lie la socket au game id


    }else{ // si deux personne on lance la partie
        nb_player = 0;
        hostPrepareGame(this);// sinon on lance la partie
    }
};


/*
 * On signal à l'attaquant que le defenseur à accepter le combat
 * param : data contient l'id du game id
 */
function lie_attack_socketGame(data) {
    this.join(data.gameID); // je lie la socket au gameID
    var data = {
        gameId : data.gameID,
        id_player : data.id_attack,
        type : 1 // signifie en mode attaque
    };
     choose_player.push(data);

}

/*
 * Dans la partie map quand on actualiste on relie la socket à lid game
 * param : data contient l'id du game id
 */
function lie_attack_socketGame_player(data) {
    console.log('lie_attack_socketGame_player')

    console.log(data)
    console.log('lie_attack_socketGame_player')

    this.join(data.gameID); // je lie la socket au gameID

}

/*
 * Partie Room
 */
/*
 * Gère l'envois des troupes selectionné par le joueur
 * @param gameId The game ID / room ID
 */
function set_armies(data) {
    this.join(data.idGame.toString()); // Si on actualise la page on lie bien de nouveau la socket



    // attribution des positon des unités
    var finish_tab = [];// Ce tableau contient tous sur les unité leurs position le type etc
    // on recrée un tableau à partir des unité choisies
    // on recherche de quel type est le joueur 0 = def 1 = attaque
    var type = parseInt(data.type);
    var y = 4;



    console.log('set_armies');
    console.log(data);
    console.log('set_armies');


    var length = (data.choose.length)-1;
    for(var i = 0; i <= length;i++ ){
        switch (data.choose[i].value) {
            case 1://Peasant
                if(type){
                    var avatar_obj = new class_warriors.Peasants(1);
                    avatar_obj.x = 1;
                    avatar_obj.y = y;
                    avatar_obj.id = type+'_'+i;
                    avatar_obj.url_avatar = '/bundles/rmplatform/images/loading/peasants_mirror.png';
                    avatar_obj.url_icone = '/bundles/rmplatform/images/guerriers-miniatures/paysan-rouge.png';
                    avatar_obj.attaquant_player = true;

                    finish_tab.push(avatar_obj);
                }else{
                    var avatar_obj = new class_warriors.Peasants(1);
                    avatar_obj.x = 10;
                    avatar_obj.y = y;
                    avatar_obj.id = type+'_'+i;
                    avatar_obj.url_avatar = '/bundles/rmplatform/images/loading/peasants.png';
                    avatar_obj.url_icone = '/bundles/rmplatform/images/guerriers-miniatures/paysan-bleu.png';
                    avatar_obj.attaquant_player = false;
                    finish_tab.push(avatar_obj);
                }
                y = y+1;

                break;
            case 2://Town_Watch
                id = data.choose[i]['id'];
                if(type){
                    var avatar_obj = new class_warriors.Town_Watch(2);
                    avatar_obj.x = 1;
                    avatar_obj.y = y;
                    avatar_obj.id = type+'_'+i;
                    avatar_obj.url_avatar = '/bundles/rmplatform/images/loading/town_watch_mirror.png';
                    avatar_obj.url_icone = '/bundles/rmplatform/images/guerriers-miniatures/milice-rouge.png';
                    avatar_obj.attaquant_player = true;

                    finish_tab.push(avatar_obj);
                }else{
                    var avatar_obj = new class_warriors.Town_Watch(2);
                    avatar_obj.x = 10;
                    avatar_obj.y = y;
                    avatar_obj.id = type+'_'+i;
                    avatar_obj.url_avatar = '/bundles/rmplatform/images/loading/town_watch.png';
                    avatar_obj.url_icone = '/bundles/rmplatform/images/guerriers-miniatures/milice-bleu.png';
                    avatar_obj.attaquant_player = false;

                    finish_tab.push(avatar_obj);
                }
                y = y+1;

                break;
            case 4://Town_Archer_Auxilia
                if(type){

                    var avatar_obj = new class_warriors.Town_Archer_Auxilia(3);
                    avatar_obj.x = 10;
                    avatar_obj.y = y;
                    avatar_obj.id = type+'_'+i;
                    avatar_obj.url_avatar = '/bundles/rmplatform/images/loading/town_archer_auxilia_mirror.png';
                    avatar_obj.url_icone = '/bundles/rmplatform/images/guerriers-miniatures/guerrier_3.png';
                    avatar_obj.attaquant_player = true;


                    finish_tab.push(avatar_obj);
                }else{
                    var avatar_obj = new class_warriors.Town_Archer_Auxilia(3);
                    avatar_obj.x = 1;
                    avatar_obj.y = y;
                    avatar_obj.id = type+'_'+i;
                    avatar_obj.url_avatar = '/bundles/rmplatform/images/loading/town_archer_auxilia.png';
                    avatar_obj.url_icone = '/bundles/rmplatform/images/guerriers-miniatures/guerrier_3.png';
                    avatar_obj.attaquant_player = false;

                    finish_tab.push(avatar_obj);
                }
                y = y+1;

                break;
            case 3://ArcherWatch
                if(type){
                    var avatar_obj = new class_warriors.Archer_Watch(4);
                    avatar_obj.x = 1;
                    avatar_obj.y = y;
                    avatar_obj.id = type+'_'+i;
                    avatar_obj.url_avatar = '/bundles/rmplatform/images/loading/archer_watch_mirror.png';
                    avatar_obj.url_icone = '/bundles/rmplatform/images/guerriers-miniatures/archer-rouge.png';
                    avatar_obj.attaquant_player = true;

                    finish_tab.push(avatar_obj);
                }else{
                    var avatar_obj = new class_warriors.Archer_Watch(4);
                    avatar_obj.x = 10;
                    avatar_obj.y = y;
                    avatar_obj.id = type+'_'+i;
                    avatar_obj.url_avatar = '/bundles/rmplatform/images/loading/archer_watch.png';
                    avatar_obj.url_icone = '/bundles/rmplatform/images/guerriers-miniatures/archer-bleu.png';
                    avatar_obj.attaquant_player = false;

                    finish_tab.push(avatar_obj);
                }
                y = y+1;

                break;
            default:
                break;
        }
    }

    choose_player.push({ 'choice':finish_tab, 'idPlayer': data.idPlayer,'idGame': parseInt(data.idGame),'type':type }); // on associe les unités au bon joueur

    this.emit('map');// on envois les infos pour rediriger sur la map de combat

    /*  for(var i = 0; i <= (choose_player.length)-1;i++ ){


     if( data.idPlayer == choose_player[i]['id_player'] && data.idGame == choose_player[i]['gameId']  ){ // on retrouve le joueur dans notre tableau et lui associe ses troupes
     choose_player[i]['choose'] = finish_tab; // on associe les unités au bon joueur
     this.emit('map',choose_player[i] );// on envois les infos pour rediriger sur la map de combat
     break;
     }
     }
     */

}
/*
 * Partie Map
 */
/*
 * 
 * @param gameId The game ID / room ID
 */

function sendColision(data) {
    var unities_fight = [];
    var found = 0;

    var dataset = choose_player;

    //On recup le tableau du player en question
    var tab_player = dataset.filter(function(val){
        if(val.idGame == data.idGame ){
            return val.idGame;
        }
    });
    console.log('data colision debut');
    console.log(data);
    console.log('data colision fin');
    //retourne lobj de ladv
    for(var key in tab_player) {
        for(var keyys in tab_player[key].choice) {

            if(tab_player[key].choice[keyys].pts_life > 0 ){
                if(tab_player[key].choice[keyys].x == data.x && tab_player[key].choice[keyys].y == data.y){
                    tab_player[key].choice[keyys].attaquant = false;

                    unities_fight.push({defense : tab_player[key].choice[keyys] });

                }
                if(tab_player[key].choice[keyys].id == data.idChoice){
                    tab_player[key].choice[keyys].attaquant = true;
                    unities_fight.push({attack : tab_player[key].choice[keyys] });
                    tab_player[key].choice[keyys].x = data.x_attack;
                    tab_player[key].choice[keyys].y = data.y_attack;
                }
            }

        }

    }

    if(unities_fight.length > 0){
        console.log('unities_fight')
        console.log(unities_fight)
        console.log('unities_fight')

        var result = calculFight(unities_fight,data.bool_corps,data.idGame);
    }


    console.log('result');
    console.log(result);
    console.log('result');


    io.sockets.in(data.idGame.toString()).emit('sendResultFight',result);// on envoi la communication sur la bonne chambre qui correspond au game id

    return;




}
//Corps permet de savoir si on est en mode corps à corps ou distance
function calculFight(tab_unities,corps,gameID) {

    var resultat = [];
    var id_attack;
    var id_defense;
    var attack_0;
    var attack_1;
    var defense_0;
    var defense_1;
    var life_0;
    var life_1;

    for(var i = 0; i <= (tab_unities.length)-1;i++ ){
        if(("attack" in tab_unities[i])){
            console.log('on deplace');
            tab_unities.move(0,i);//Ici on sassure que lattaquant est en premier
        }
    }



    if(corps == true){

        attack_0 =  Math.round((tab_unities[0].attack.pts_attack /100)* Math.random() * ( parseInt(tab_unities[0].attack.pts_attack + ( parseInt( 100 - tab_unities[1].defense.pts_defense ))) - tab_unities[0].attack.pts_attack)+ tab_unities[0].attack.pts_attack )+ Math.random() * ( 10 + 1 ) ;
        attack_1 =  Math.round((tab_unities[1].defense.pts_attack /100)*  Math.random() * ( parseInt(tab_unities[1].defense.pts_attack +( parseInt( 100 - tab_unities[0].attack.pts_defense ))) - tab_unities[1].defense.pts_attack) + tab_unities[1].defense.pts_attack );

       /* defense_0 =  Math.round((tab_unities[0].attack.pts_defense /100)* Math.floor(Math.random() * (30 - 0 + 1)) + 0);
        defense_1 =  Math.round((tab_unities[1].defense.pts_defense /100)* Math.floor(Math.random() * (30 - 0 + 1)) + 0);*/
    }else{
        console.log('attaque a ditance')
        //Une personne de corps a corps se fait attaquer a distance
        if(tab_unities[0].attack.pts_attack_distance == 0){
            attack_0 = 0;

        }else{
            //Seul l'attaquant peut attaquer
            attack_0 =  Math.round((tab_unities[0].attack.pts_attack /100)* Math.random() * ( parseInt(tab_unities[0].attack.pts_attack + ( parseInt( 100 - tab_unities[1].defense.pts_defense ))) - tab_unities[0].attack.pts_attack) + tab_unities[0].attack.pts_attack );

        }

        attack_1 = 0;

    }



    if(attack_0 < 0){
        attack_0 = 0
    }
    if(attack_1 < 0){
        attack_1 = 0
    }

    //Diff de vie de plus de 60 ou egale on subit des degats supplementaires
    if((Math.abs(tab_unities[0].attack.pts_life - tab_unities[1].defense.pts_life) >= 60)){

        if(tab_unities[0].attack.pts_life > tab_unities[1].defense.pts_life ){

            attack_0 +=  Math.round(Math.random() * (25 - 5) + 5)
        }else{
            attack_1 +=  Math.round(Math.random() * (25 - 5) + 5)
        }

    }

    life_0 =  Math.round((tab_unities[0].attack.pts_life)- attack_1); // name de la personne qui est attaque
    life_1 = Math.round((tab_unities[1].defense.pts_life)- attack_0);

    console.log(life_0)
    console.log(life_1)


    var data_2 = {
        id : tab_unities[0].attack.id, // id de celle qui recois
        life : life_0, // name de la personne qui est attaque
        x : tab_unities[0].attack.x, // name de la personne qui est attaque
        y : tab_unities[0].attack.y // name de la personne qui est attaque
    };
    resultat.push(data_2);
    var data_2 = {
        id : tab_unities[1].defense.id, // id de celle qui recois
        life : life_1, // name de la personne qui est attaque
        x : tab_unities[1].defense.x, // name de la personne qui est attaque
        y : tab_unities[1].defense.y // name de la personne qui est attaque
    };
    resultat.push(data_2);


    var death_attack = 0;
    var death_defense = 0;
    for(var i = 0; i <= (choose_player.length)-1;i++ ){


        if(choose_player[i]['idGame'] == gameID){ // on retrouve le choix dunité des joueurs selon le game ID
            console.log('choose_player[i] debut')
            console.log(choose_player[i]['idPlayer'])
            console.log('choose_player[i] fin')



            var tail_tab_unities = choose_player[i].choice.length;
            for (var ii = 0; ii <= (tail_tab_unities)-1; ii++) {


                console.log(' debut choose_player[i].choice[ii]')
                console.log(choose_player[i].choice[ii]);
                console.log(' fin choose_player[i].choice[ii]')


                //On assigne leur nouvelle sante
                if(choose_player[i].choice[ii].id == tab_unities[0].attack.id ){

                    if(life_0 <= 0){
                        choose_player[i].choice[ii].url_icone = '/bundles/rmplatform/images/guerriers-miniatures/guerrier_dead.png';
                    }

                    choose_player[i].choice[ii].pts_life = life_0;
                    choose_player[i].choice[ii].move_case = false; // on signal quil ne peut plus se déplacer
                    choose_player[i].choice[ii].attack_ok = false; // on signal quil ne peut plus attaquer




                }
                if(choose_player[i].choice[ii].id == tab_unities[1].defense.id ){
                    if(life_1 <= 0){
                        choose_player[i].choice[ii].url_icone = '/bundles/rmplatform/images/guerriers-miniatures/guerrier_dead.png';
                    }

                    choose_player[i].choice[ii].pts_life = life_1;

                }
                //On controlle ici le nb d'unite encore en vie


                if(choose_player[i].choice[ii].attaquant_player == true){
                    console.log(11)
                    if(choose_player[i].choice[ii].pts_life <= 0){
                        death_attack++;
                        id_attack = choose_player[i]['idPlayer'];


                    }
                }
                if(choose_player[i].choice[ii].attaquant_player == false){
                    if(choose_player[i].choice[ii].pts_life <= 0){
                        death_defense++;
                        id_defense= choose_player[i]['idPlayer'];

                    }
                }




            }

        }
    }

    console.log('death_attack');
    console.log(death_attack);


    console.log('death_defense');
    console.log(death_defense);
    if(death_attack >= 4){
        if(death_defense < 4){
            //Victoire on stop la partie
            resultat.push({'victoire': id_defense});

            var data_stop = {
                idGame :gameID // id de celle qui recois
            };
            stopGame(data_stop);

        }
    }
    if(death_defense >= 4){
        if(death_attack < 4){
            resultat.push({'victoire': id_attack});

        }
    }
    if(death_defense == 4 && death_attack == 4  ){
        resultat.push({'egalite': 'egalite'});
        var data_stop = {
            idGame :gameID // id de celle qui recois
        };
        stopGame(data_stop);

    }

    return resultat;





}

/*
 * Partie Map
 */



function infoTipShow(data){
    console.log('on y est')

    var dataset = choose_player;

    //On recup le tableau du player en question
    var tab_player = dataset.filter(function(val){
        if(val.idGame == data.idGame ){
            return val.idGame;
        }
    });
    console.log('data colision debut');
    console.log(data);
    console.log('data colision fin');
    for(var key in tab_player) {
        for(var keyys in tab_player[key].choice) {

            if(tab_player[key].choice[keyys].pts_life > 0 ){
                console.log('x : '+tab_player[key].choice[keyys].x)
                console.log('y : '+tab_player[key].choice[keyys].y)

                if(tab_player[key].choice[keyys].x == data.x && tab_player[key].choice[keyys].y == data.y){
                    console.log('tab_player[key]');
                    console.log(tab_player[key].choice[keyys]);
                    console.log('tab_player[key]');
                    console.log('tab_player[type_unity]');
                    console.log(tab_player[key].choice[keyys].type_unity);
                    console.log('tab_player[type_unity]');
                    var image = ''; var name = '';

                    switch (tab_player[key].choice[keyys].type_unity) {
                        case 1://Peasant


                            image = '/bundles/rmplatform/images/army-pop/peasants.png';
                            name = 'Paysan';
                            break;
                        case 2://Town_Watch

                            image = '/bundles/rmplatform/images/army-pop/town_watch.png';
                            name = 'Milice';
                            break;
                        case 4://Town_Archer_Auxilia


                            image = '/bundles/rmplatform/images/army-pop/archer_watch.png';
                            name = 'Archer';
                            break;
                        case 3://ArcherWatch



                            break;
                        default:
                            break;
                    }


                    this.emit('set_infoTipShow',{life:tab_player[key].choice[keyys].pts_life, src : image,name : name});

                    return false;

                }

            }

        }

    }

}


/*
 * On demarre le compteur d'attente de joueur et de choix de garnison
 * @param gameId The game ID / room ID
 */

function compteur(data) {
    var found = false;
    var time;
    var date = new Date();

    var tail_tab = tab_compteur_team.length;
    //this.join(data.idGame);// on donne à la socket le bonne id pour envoyer l'info aux bonne chambre
    for(var i = 0; i <= tail_tab-1;i++ ){
        if(data.idGame == tab_compteur_team[i]['idGame'] ){

            console.log('on a trouve');

            time = tab_compteur_team[i]['time'];
            found = true;
            break;
        }
    }
    if(!found){ // on demarre le compteur

        console.log('pas trouvé');
        var launch = new Date(date.getFullYear(),date.getMonth(),date.getDate(),date.getHours(),date.getMinutes()+2,date.getSeconds());
        time = launch;
        var data = {
            time : launch,
            idGame : data.idGame,
            type : 0, // signifie en mode def
            empty : 0 // variable du nb de fois de non joué dans la partie
        };

        tab_compteur_team.push(data);//le compteur à l'id de la session + le tempo
    }
    console.log(time);
    //  io.sockets.in(data.idGame).emit('setCompteur', time);// on envoi la communication sur la bonne chambre qui correspond au game id
    this.emit('setCompteur',{ time_end : time, time_start :date} );//1er param la date de fin,2eme param la date de debut
    //   this.broadcast.emit('setCompteur', time); //on envois l'info � tous le monde
}
/*
 * Le joueur n'a pas joué on comptabilise c'est passage a blanc et on change le tour
 * @param gameId The game ID / room ID
 */

function passTour(data) {
    var pass;
    var tail_tab = tab_compteur_team.length;
    console.log('data')
    console.log(data)
    console.log('data')
    // if(asynchrone){

    //this.join(data.idGame);// on donne à la socket le bonne id pour envoyer l'info aux bonne chambre
    for(var i = 0; i <= tail_tab-1;i++ ){
        if(data.idGame == tab_compteur_team[i]['idGame'] ){
            tab_compteur_team[i]['empty'] = (parseInt(tab_compteur_team[i]['empty']) +1) ;
            pass = tab_compteur_team[i]['empty'];
            //Il passe son tour on regarde si il est toujours la
            controlPlayerOnline(data.id_generate,data.idGame);



            break;
        }
    }
    //Partie termine
    if (typeof data.endgame != 'undefined'){
        stopGame(data);
    }




    //A FAIRE SI PASS SUP A 3 FIN DU BAL ON ARRETE LA

    //asynchrone = false;
     if(pass >= 3){
         stopGame(data);
         io.sockets.in(data.idGame.toString()).emit('redirect_room');// on envoi la communication sur la bonne chambre qui correspond au game id
    }else{
        asynchrone = true;
     }



}
/*
 * On démarre le compteur apres un passement de tour
 * @param gameId The game ID / room ID
 */

function initCompteur(data) {
    console.log("initCompteur")
    var found = false;
    var time;
    var tail_tab = tab_compteur_team.length;
    var date = new Date();
    //this.join(data.idGame);// on donne à la socket le bonne id pour envoyer l'info aux bonne chambre
    for(var i = 0; i <= tail_tab-1;i++ ){
        if(data.idGame == tab_compteur_team[i]['idGame'] ){
            console.log('trouvé')
            console.log(tab_compteur_team[i])
            var launch = new Date(date.getFullYear(),date.getMonth(),date.getDate(),date.getHours(),date.getMinutes()+2,date.getSeconds());
            tab_compteur_team[i]['time'] = launch;
            console.log(tab_compteur_team[i])
            time = launch;
            console.log(time);
            found = true;
            break;
        }
    }
    if(found){ // on demarre le compteur

        console.log('on passe la oklm')
        // this.emit('setCompteur', time);
        io.sockets.in(data.idGame.toString()).emit('setCompteur',{time_end : time , time_start : date });// on envoi la communication sur la bonne chambre qui correspond au game id


    }

}
/*
 * Le joueur n'as pas choisi sa garnison dans le temps imparti on supprime le compteur et la partie code fait pour la partie team
 * @param gameId The game ID / room ID
 */

function failCompteur(data) {

    delTime(data);//Suppresion du time dans le tableau
    delGame(data);//Suppresion de la partie dans le tableau
    console.log()
    //this.emit('redirect_room');
    console.log('failCompteur')
    console.log(data)
    //io.sockets.in(data.idGame.toString()).emit('redirect_room');// on envoi la communication sur la bonne chambre qui correspond au game id
    this.emit('redirect_room');// on envoi la communication sur la bonne chambre qui correspond au game id


}
/*
 * Le joueur n'as pas choisi sa garnison dans le temps imparti on supprime le compteur et la partie fin de partie pour la partie map
 * @param gameId The game ID / room ID
 */

function stopGame(data) {

    delTime(data);//Suppresion du time dans le tableau
    delAvatar(data);//Suppresion du time dans le tableau
    delPlayerOnlineBattle(data);
    delobjTour(data);
    // delGame(data);//Suppresion de la partie dans le tableau

    // this.emit('redirect_room');


}

/*
 * Gère l'envois des troupes selectionné par le joueur
 * @param gameId The game ID / room ID
 */
function positionOfUnity(data) {
    console.log('salut')
    this.join(data.idGame.toString()); // Si on actualise la page on lie bien de nouveau la socket

    //console.log(choose_player[0][0]);

    /*    for(var i = 0; i <= (choose_player.length)-1;i++ ){
     if( data.idGame == choose_player[i]['idGame']   ){ // on retrouve le choix dunité des joueurs selon le game ID
     this.emit('choose_unities', {unities : choose_player[i] , etat : data.etat }  );// on envois les unités sur la map + etat qui corespond si on affiche les unité ou pas
     }
     }*/
    var unities_player = [];
    var player_found = 0;
    for(var i = 0; i <= (choose_player.length)-1;i++ ){
        if( data.idGame == choose_player[i]['idGame']  ){ // on retrouve le choix dunité des joueurs selon le game ID
            player_found++;
            unities_player.push(choose_player[i]);
            if(player_found >= 2){
                console.log('ok')
                console.log(unities_player);
                this.emit('choose_unities', {unities : unities_player , etat : data.etat }  );// on envois les unités sur la map + etat qui corespond si on affiche les unité ou pas
                break;
            }
        }
    }

}
/*
 * Recupere la position de lavatar selectionné
 * @param gameId The game ID / room ID
 */
function setPositionUnity(data) {
    var x;
    var y;
    var type;
    var found = false;
    for(var i = 0; i <= (choose_player.length)-1;i++ ){

        if( data.idPlayer == choose_player[i]['id_player'] && data.idGame == choose_player[i]['gameId']  ){ // on retrouve le choix dunité des joueurs selon le game ID
            console.log('On a trouve le player')
            console.log(choose_player[i]['choose'])
            for(var ii = 0; ii <= (choose_player[i]['choose'].length)-1;ii++  ){
                if(choose_player[i]['choose'][ii]['id'] == data.idUnity){
                    x = choose_player[i]['choose'][ii]['x'];
                    y = choose_player[i]['choose'][ii]['y'];
                    type = choose_player[i]['type'] // On renvois le type si def ou atta
                    found = true;
                    break;
                }
            }
        }
    }
    this.emit('position_unitie', { x_avatar : x, y_avatar : y ,idUnity : data.idUnity, type_player : type } );// on envois les unités sur la map
}
/*
 * Recupere la position de lavatar selectionné
 * @param gameId The game ID / room ID
 */
function collision(data) {
    console.log('collision bien signalé')
}
/*
 * On enregistre la nouvelle position
 * @param gameId The game ID / room ID
 */
function setPosition(tab_val) {
    console.log('new position')
    var tail_tab = choose_player.length;

    for (var i = 0; i <= (tail_tab-1); i++) {
        if(tab_val.idPlayer == choose_player[i].idPlayer && tab_val.idGame == choose_player[i].idGame  ){

            console.log('choose_player[i].idGame')
            console.log(choose_player[i].idGame)
            console.log('choose_player[i].idGame')

            console.log('choose_player[i].idGame')
            console.log(choose_player[i].idGame)
            console.log('choose_player[i].idGame')


            var tail_tab_unities = choose_player[i].choice.length;
            for (var ii = 0; ii <= (tail_tab_unities)-1; ii++) {
                if(choose_player[i].choice[ii].id == tab_val.idChoice ){

                    console.log('choose_player[i].choice[ii].id')
                    console.log(choose_player[i].choice[ii].id)
                    console.log('choose_player[i].choice[ii].id')


                    //partie save des positions
                    var old_x = choose_player[i].choice[ii].x;
                    var old_y = choose_player[i].choice[ii].y;
                    choose_player[i].choice[ii].x = tab_val.x;
                    choose_player[i].choice[ii].y = tab_val.y;
                    choose_player[i].choice[ii].move_case = false;//On bloque le deplacement




                    //  this.broadcast.emit('newPosition', {tab:choose_player[i].choice[ii],old_x:old_x,old_y:old_y} ); //on envois l'info de deplacement à ladversaire
                    this.broadcast.to(tab_val.idGame).emit('newPosition', {tab:choose_player[i].choice[ii],old_x:old_x,old_y:old_y} ); //on envois l'info de deplacement à ladversaire

                    // socket.broadcast.to('game').emit('message', 'nice game');
                    break;
                }
            }
        }
    }
    //NON FONCTIONEL POUR LE MOMENT JE SAIS PAS PK
    /* var dataset = choose_player;

     //On recup le tableau du player en question
     var occurances = dataset.filter(function(val){
     if(val.idGame == tab_val.idGame && val.idPlayer == tab_val.idPlayer  ){
     return val.choice;
     }
     });
     //Ensuite on recup l'objet choice et le bon avatar
     var obj_avatar = occurances[0].choice.filter(function(val){
     return val.id == tab_val.idChoice ;
     });

     var old_x = obj_avatar[0].get_y();
     var old_y = obj_avatar[0].get_x();
     //Et enfin on fait les modifs
     obj_avatar[0].set_y(tab_val.y);
     obj_avatar[0].set_x(tab_val.x);
     obj_avatar[0].avatar_deplacable_false(); // on signal quil ne peut plus se déplacer

     this.broadcast.emit('newPosition', {tab:obj_avatar[0],old_x:old_x,old_y:old_y} ); //on envois l'info de deplacement à ladversaire */



};
/*
 * Un compteur si les deux adv sont sur la page ainsi qu'on attribu lid du game à la socket
 * @param gameId The game ID / room ID
 */
function setPlayerBegin(data) {

    console.log('setPlayerBegin');
    console.log(data);

    console.log('1')


    var found = false;

    var tail_tab_nbtour = nb_tour.length;
    this.join(data.idGame.toString()); // Si on actualise la page on lie bien de nouveau la socket

    var tab_tour = []; // contient le nb de tour et le joueur qui doit jouer

    var dataset = player_online_map;

    var search = data.idGame;
    var occurances = dataset.filter(function(val){
        if(val.idGame === search){
            return val.idGame === search;
        }
    }).length;


    var search_P = data.idPlayer;
    var player_present = dataset.filter(function(val){
        if(val.idPlayer === search_P){
            return val.idPlayer === search_P;
        }
    }).length;


    console.log('player_present');
    console.log(player_present);
    console.log('2')

    //Si il est deja enregistré on attend le deuxieme
    if(player_present >= 1){
        console.log('3')

        //Si occurances sup a 2 on sait que la partie est lancée
        if(occurances >= 2){
            console.log('4')

            //On recup l'obj du tour
            var result_tour = obj_tour.filter(function( obj ) {

                return obj.id_game == data.idGame;
            });
            this.emit('lauchGame', { start: false, detail_tour : result_tour[0] } );// on revois les données que pour notre map
            return;
        }

    }else{
        console.log('5')

        var data_2 = {
            idPlayer : data.idPlayer,
            idGame : data.idGame,
            pseudo :  data.pseudo,
            type :  data.type

        };
        /*  if(data.type){ // L'attaquant lance la partie on controle si le type est à un si c'est le cas on rempli un tableau d'info
         var data_3 = {
         idPlayer : data.idPlayer,
         idGame : data.idGame,
         tour : 1
         };
         nb_tour.push(data_3);
         }*/


        start = true;// je prepare louverture du rideau
        player_online_map.push(data_2);
        console.log(player_online_map)
        this.emit('compteur_attente',true);// on envoi la communication sur la bonne chambre qui correspond au game id
    }
    //Sinon on continu
    var occurances = dataset.filter(function(val){
        if(val.idGame === search){
            return val.idGame === search;
        }
    }).length;
    console.log('occurance');//3

    console.log('6')


    console.log(occurances);//3
    //Dans le cas ou on actualise
    if(occurances >= 2){ //Ce cas signifie que les deux joueurs sont presents

        console.log('7')

        //var obj_tour = new class_tour.TourParTour(data.idGame);

        //On recup les idlplayer
        var tail_tab = player_online_map.length;
        var tab_player_room = [];
        var ii = 0;

        for(var i = 0; i <= tail_tab-1;i++ ){
            if(data.idGame == player_online_map[i]['idGame'] ){
                tab_player_room.push(player_online_map[i])
                ii++;
            }
            if(ii >= 2){
                break;
            }
        }
        //On supprime le compteur pour en relance un nouveau vu comme la partie demarre
        var del = {idGame : data.idGame };
        delTime(del)


        console.log('tab_player_room')
        console.log(tab_player_room)
        console.log('tab_player_room')
        //On crée un obj tour pour cette partie vu comme les deux players sont présents
        var tour = new class_tour.TourParTour(tab_player_room[0].idGame,tab_player_room[0].idPlayer,tab_player_room[0].pseudo,tab_player_room[0].type,tab_player_room[1].idPlayer,tab_player_room[1].pseudo,tab_player_room[1].type);
        obj_tour.push(tour);
        io.sockets.in(data.idGame.toString()).emit('lauchGame',{ start: true, detail_tour : tour });// on envoi la communication sur la bonne chambre qui correspond au game id
        return;
    }



    return;

    //On insere lid du player comme quoi il est 
    for(var i = 0; i <= tail_tab-1;i++ ){
        if(data.idGame == player_online_map[i]['idGame'] ){
            found = true;
            break;
        }
    }
    if(!found){ // 1er arrivée
        var data_2 = {
            idPlayer : data.idPlayer,
            idGame : data.idGame,
            pseudo :  data.pseudo
        };
        if(data.type){ // L'attaquant lance la partie on controle si le type est à un si c'est le cas on rempli un tableau d'info
            var data_3 = {
                idPlayer : data.idPlayer,
                idGame : data.idGame,
                tour : 1
            };
            nb_tour.push(data_3);
        }


        start = true;// je prepare louverture du rideau
        player_online_map.push(data_2);
        this.emit('compteur_attente',true);// on envoi la communication sur la bonne chambre qui correspond au game id

    }else{
        //1er joueur est deja present on ecarte le rideau est la partie peux commencer
        var data_2 = {
            idPlayer : data.idPlayer,
            idGame : data.idGame,
            start : true,
            pseudo :  data.pseudo

        };
        if(data.type){ // L'attaquant lance la partie on controle si le type est à un si c'est le cas on rempli un tableau d'info
            var data_3 = {
                idPlayer : data.idPlayer,
                idGame : data.idGame,
                tour : 1
            };
            nb_tour.push(data_3);
        }
        //On rajoute le dernier pseudo
        tab_pseudo.push(data.pseudo)
        player_online_map.push(data_2);
        for(var i = 0; i <= tail_tab_nbtour-1;i++ ){
            if(data.idGame == nb_tour[i]['idGame'] ){
                tab_tour = nb_tour[i];
                break;
            }
        }
        //Une fois le deuxieme player arrive j'ouvre le rideau il est resté à true
        io.sockets.in(data.idGame.toString()).emit('lauchGame',{ start: true, tab_pseudo : tab_pseudo, detail_tour : tab_tour });// on envoi la communication sur la bonne chambre qui correspond au game id
    }
}
/*
 * Compteur tour par tour de la map
 * @param gameId The game ID / room ID
 */

function compteurTourParTour(data) {
    var found = false;
    var time;
    var tail_tab = tab_compteur_team.length;
    this.join(data.idGame);// on donne à la socket le bonne id pour envoyer l'info aux bonne chambre
    for(var i = 0; i <= tail_tab-1;i++ ){
        if(data.idGame == tab_compteur_team[i]['idGame'] ){
            time = tab_compteur_team[i]['time'];
            found = true;
            break;
        }
    }
    if(!found){ // on demarre le compteur
        var date = new Date();
        var launch = new Date(date.getFullYear(),date.getMonth(),date.getDate(),date.getHours(),date.getMinutes(),date.getSeconds());
        time = launch;
        var data = {
            time : launch,
            idGame : data.idGame,
            type : 0 // signifie en mode def
        };

        tab_compteur_team.push(data);//le compteur à l'id de la session + le tempo
    }
    //io.sockets.in(data.idGame).emit('setCompteur', time);// on envoi la communication sur la bonne chambre qui correspond au game id
    this.emit('setCompteur', time);

}


/*
 * Valide le tour est passe au suivant si la fonctino est appele aapres un passage de tour le param pass est a true
 * @param gameId The game ID / room ID
 */
function validateTour(data) {
    console.log('data');
    console.log(data);
    console.log('validateTour');
    console.log(data.pass);

    var player_move = test_player_move(data.id_generate,data.idGame);

    console.log(player_move);
    if(player_move || data.pass){
        console.log('on peut valider')

        if(data.asynchrone){


        var result_tour = obj_tour.filter(function( obj ) {
            return obj.id_game == data.idGame;
        });


            if(result_tour.length > 0 ){


                result_tour[0].increase_tour();//On incremente le tour

                //On change la personne qui doit jouer
                if(data.type_play == 1){result_tour[0].type_play_game = 0;}else{result_tour[0].type_play_game = 1;}

                var dataset = choose_player;

                var occurances = dataset.filter(function(val,i){
                    if(val.idGame == data.idGame ){
                        var obj_avatar = val.choice.filter(function(val){
                            val.avatar_deplacable_true(); // On remet a true le deplacement
                            val.avatar_attack_true(); // On remet a true les attaques

                        });
                    }
                });


             //   delAvatar(data);


                console.log('on balance le nb de tour')
                console.log(result_tour[0])
                io.sockets.in(data.idGame).emit('tourOk', result_tour[0]);// on envoi la communication sur la bonne chambre qui correspond au game id

            }


        }
    }else{

        var message = '<div class="alert alert-dismissible notif"> <button class="close" data-dismiss="alert">&times;</button> Vous devez deplacer au moins une unité.</div>';
        this.emit('list_error',{value : 2, message : message});

    }
    //Permet de gerer l'erreur ou les tour s'incremente de deux car les deux pages appelent cette methonde
    console.log(asynchrone)
    console.log(1)
    /* if(!data.asynchrone){
     console.log(2)

     if(asynchrone == true){
     var result_tour = obj_tour.filter(function( obj ) {
     return obj.id_game == data.idGame;
     });
     result_tour[0].increase_tour();//On incremente le tour

     //On change la personne qui doit jouer
     if(data.type_play == 1){result_tour[0].type_play_game = 0;}else{result_tour[0].type_play_game = 1;}

     var dataset = choose_player;

     var occurances = dataset.filter(function(val,i){
     if(val.idGame == data.idGame ){
     var obj_avatar = val.choice.filter(function(val){
     val.avatar_deplacable_true(); // On remet a true le deplacement
     val.avatar_attack_true(); // On remet a true les attaques

     });
     }
     });
     asynchrone = false;
     console.log(asynchrone)
     console.log(3)

     io.sockets.in(data.gameId).emit('tourOk', result_tour[0]);// on envoi la communication sur la bonne chambre qui correspond au game id

     }else{
     console.log(4)
     asynchrone = true;
     console.log(asynchrone)
     }



     }*/








}
/*
 * Permet de savoir si la personne qui valide a déplacer au moin une fois un perso
 * @param gameId The game ID / room ID
 */
function test_player_move(id_generate,id_game){
    var tail_tab = choose_player.length;

    for (var i = 0; i <= (tail_tab-1); i++) {
        if(id_generate == choose_player[i].idPlayer && id_game == choose_player[i].idGame  ){

            var tail_tab_unities = choose_player[i].choice.length;
            for (var ii = 0; ii <= (tail_tab_unities)-1; ii++) {

                if(choose_player[i].choice[ii].move_case == false ){
                    console.log('trouvéééééé')
                    //Il y a au moins une unité de deplacé il peut donc valider le tour
                    return true;
                }
            }
        }
    }
    //On a rien trouvé on empeche la validation du tour
    return false;
}

/*
 * Two players have joined. Alert the host!
 * @param gameId The game ID / room ID
 */
function hostPrepareGame(obj) {
    var sock = obj;
    var data = {
        mySocketId : sock.id,
        gameId : gameId,
        type : 'defender'
    };
    tab_game.push(data);
    sock.join(gameId.toString()); // on lit la socket au game id

    io.sockets.in(gameId).emit('newGameCreated', tab_game);// on envoi la communication sur la bonne chambre qui correspond au game id

}

function initSocket(game_id_client) { // cette methode est envoy� par map.php quand elle initialise elle permet de lier le chambres correctement
    this.join(game_id_client.toString());
    console.log('aaa');
    io.sockets.in(game_id_client).emit('send_gameID', game_id_client);// on envoi l'id � pp3diso
};
/*
 * Utilities methode de suppresion des champs dans les tableaux
 *
 */

function delTime(data) {
    var tail_tab = tab_compteur_team.length;

    console.log('tab_compteur_team');
    console.log(tab_compteur_team);
    console.log('tab_compteur_team');
    console.log('data');
    console.log(data);
    console.log('data');

    tab_compteur_team = tab_compteur_team
        .filter(function (el) {
                return el.idGame != data.idGame;
            }
        );

   /* for(var i = 0; i <= tail_tab-1;i++ ){
        if(data.idGame == tab_compteur_team[i]['idGame'] ){
            //On supprime le compteur lié au gameID
            console.log('suppresion')
            tab_compteur_team.splice(i,1);
           // break;
        }
    }*/
    console.log('tab_compteur_team');
    console.log(tab_compteur_team);
    console.log('tab_compteur_team');

};
//Inutile
function delGame(data) {
    var tail_tab = tab_players.length;
    for(var i = 0; i <= tail_tab-1;i++ ){
        if(data.id_generate == tab_players[i]['id_generate'] ){
            tab_players.splice(i,1);

            break;
        }
    }

};

//Suppresion des guerriers choisis
function delAvatar(data) {

    console.log('choose_player');
    console.log(choose_player);
    console.log('choose_player');

    console.log('data');
    console.log(data);
    console.log('data');


    choose_player = choose_player
        .filter(function (el) {
                return el.gameId != data.idGame;
            }
        );
    choose_player = choose_player
        .filter(function (el) {
                return el.idGame != data.idGame;
            }
        );


    console.log('choose_player');
    console.log(choose_player);
    console.log('choose_player');


};
//Suppresion des joueurs en ligne du combat
function delPlayerOnlineBattle(data) {

    console.log('data');
    console.log(data);
    console.log('data');
    console.log('player_online_map');
    console.log(player_online_map);
    console.log('player_online_map');
    var found = 0;


    player_online_map = player_online_map
        .filter(function (el) {
                return el.idGame != data.idGame;
            }
        );



    console.log('player_online_map');
    console.log(player_online_map);
    console.log('player_online_map');

};
//Suppresion des joueurs en ligne du combat
function delobjTour(data) {
    console.log('obj_tour');
    console.log(obj_tour);
    console.log('obj_tour');


    obj_tour = obj_tour
        .filter(function (el) {
                return el.id_game != data.idGame;
            }
        );


    /*var tail_tab = obj_tour.length;
    for(var i = 0; i <= tail_tab-1;i++ ){
        if(data.idGame == obj_tour[i]['id_game'] ){
            obj_tour.splice(i,1);

            break;
        }
    }*/
    console.log('obj_tour');
    console.log(obj_tour);
    console.log('obj_tour');
};



Array.prototype.move = function (old_index, new_index) {
    if (new_index >= this.length) {
        var k = new_index - this.length;
        while ((k--) + 1) {
            this.push(undefined);
        }
    }
    this.splice(new_index, 0, this.splice(old_index, 1)[0]);
    return this; // for testing purposes
};