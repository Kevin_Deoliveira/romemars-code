var __extends = (this && this.__extends) || function (d, b) {
        for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
var Sayings;
(function (Sayings) {
    var Warrior = (function () {
        function Warrior(type_unity) {
            this.type_unity = type_unity;
            this.x = 0;
            this.y = 0;
            this.id;
            this.move_case = true;
            this.pts_life = 100;
            this.attaquant = false;
            this.attaquant_player = false;
            this.url_icone;
            this.url_avatar;
            this.attack_ok = true;
        }
        Warrior.prototype.set_y = function (position_y) {
            this.y = position_y;
        };
        Warrior.prototype.get_y = function () {
            return this.y;
        };
        Warrior.prototype.set_x = function (position_x) {
            this.x = position_x;
        };
        Warrior.prototype.get_x = function () {
            return this.x;
        };
        Warrior.prototype.avatar_deplacable_true = function () {
            this.move_case = true;
        };
        Warrior.prototype.avatar_deplacable_false = function () {
            this.move_case = false;
        };
        Warrior.prototype.avatar_attack_true = function () {
            this.attack_ok = true;
        };
        Warrior.prototype.avatar_attack_false = function () {
            this.attack_ok = false;
        };
        return Warrior;
    }());
    Sayings.Warrior = Warrior;
    var Peasants = (function (_super) {
        __extends(Peasants, _super);
        function Peasants(type_unity) {
            _super.call(this, type_unity);
            this.move_max = 4;
            this.pts_armure = 20; // inutile pour le moment
            this.pts_attack = 25;
            this.pts_defense = 20;
            this.pts_attack_distance = 0;
            this.pts_defense_distance = 15;
            this.distance = false;
            this.portee_max = 0;
        }
        return Peasants;
    }(Warrior));
    Sayings.Peasants = Peasants;
    var Town_Watch = (function (_super) {
        __extends(Town_Watch, _super);
        function Town_Watch(type_unity) {
            _super.call(this, type_unity);
            this.move_max = 3;
            this.pts_armure = 35;
            this.pts_attack = 35;
            this.pts_defense = 35;
            this.pts_attack_distance = 0;
            this.pts_defense_distance = 30;
            this.distance = false;
            this.portee_max = 0;
        }
        return Town_Watch;
    }(Warrior));
    Sayings.Town_Watch = Town_Watch;
    var Town_Archer_Auxilia = (function (_super) {
        __extends(Town_Archer_Auxilia, _super);
        function Town_Archer_Auxilia(type_unity) {
            _super.call(this, type_unity);
            this.move_max = 4;
            this.pts_armure = 35;
            this.pts_attack = 15;
            this.pts_defense = 15;
            this.pts_attack_distance = 25;
            this.pts_defense_distance = 30;
            this.distance = true;
            this.portee_max = 4;
        }
        return Town_Archer_Auxilia;
    }(Warrior));
    Sayings.Town_Archer_Auxilia = Town_Archer_Auxilia;
    var Archer_Watch = (function (_super) {
        __extends(Archer_Watch, _super);
        function Archer_Watch(type_unity) {
            _super.call(this, type_unity);
            this.move_max = 3;
            this.pts_armure = 20;
            this.pts_attack = 15;
            this.pts_defense = 20;
            this.pts_attack_distance = 25;
            this.pts_defense_distance = 15;
            this.distance = true;
            this.portee_max = 2;
        }
        return Archer_Watch;
    }(Warrior));
    Sayings.Archer_Watch = Archer_Watch;
})(Sayings || (Sayings = {}));
module.exports = Sayings;
