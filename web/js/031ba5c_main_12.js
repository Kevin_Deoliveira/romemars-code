$(document).ready(function() {

      /*
    * Gestion des changements d'images de guerriers
    */
     $( ".top_guerrier" ).on( "click", function() {
         var lien_image = $(this).siblings()[0].src;
         var new_img = lien_image.split("_");
         var num = parseInt(new_img[1].substr(0, 1));// contient le num de l'image
         var new_num;
         if(num > 3){
             new_num = 1
         }else{
            new_num = parseInt( num+1 ) ;
         }
       $(this).siblings()[0].dataset.value = new_num; // indique l'id de limage
       $(this).siblings()[0].src = new_img[0]+'_'+new_num+'.png';
    }); 
     $( ".bottom_guerrier" ).on( "click", function() {
         var lien_image = $(this).siblings()[1].src;
         var new_img = lien_image.split("_");
         var num = parseInt(new_img[1].substr(0, 1));// contient le num de l'image
         var new_num;
         if(num < 2){
             new_num = 4
         }else{
            new_num = parseInt( num-1 ) ;
         }
       $(this).siblings()[1].dataset.value = new_num;
       $(this).siblings()[1].src = new_img[0]+'_'+new_num+'.png';
    }); 

});

$(function(){
    $('.menu').slimScroll({
     
    });

});
$(function() {

    // Call Gridder
    $('.gridder').gridderExpander({
        scroll: true,
        scrollOffset: 30,
        scrollTo: "panel",                  // panel or listitem
        animationSpeed: 400,
        animationEasing: "easeInOutExpo",
        showNav: true,                      // Show Navigation
        nextText: "Next",                   // Next button text
        prevText: "Previous",               // Previous button text
        closeText: "Close",                 // Close button text
        onStart: function(){
            //Gridder Inititialized
        },
        onContent: function(){
            //Gridder Content Loaded
        },
        onClosed: function(){
            //Gridder Closed
        }
    });

});
