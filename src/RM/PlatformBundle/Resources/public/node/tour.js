var Tour;
(function (Tour) {
    var TourParTour = (function () {
        function TourParTour(id_game, id_player_1, pseudo_1, type_1, id_player_2, pseudo_2, type_2) {
            this.id_game = id_game;
            this.id_player_1 = id_player_1;
            this.pseudo_1 = pseudo_1;
            this.type_1 = type_1;
            this.id_player_2 = id_player_2;
            this.pseudo_2 = pseudo_2;
            this.type_2 = type_2;
            this.number_tour = 1;
            this.type_play_game = 1;
        }
        TourParTour.prototype.increase_tour = function () {
            this.number_tour++;
        };
        return TourParTour;
    })();
    Tour.TourParTour = TourParTour;
})(Tour || (Tour = {}));
module.exports = Tour;
